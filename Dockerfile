FROM jupyter/minimal-notebook:lab-3.4.2
ENV DEBIAN_FRONTEND noninteractive
#ENV DEBCONF_NONINTERACTIVE_SEEN true

#RUN apt-get clean
#RUN apt-get autoclean
USER root

RUN apt-get update
RUN apt-get install -y python3-dev build-essential  

RUN apt-get install -y python3-pip wget iputils-ping openssh-server
RUN pip3 install streamlit pandas matplotlib numpy 
EXPOSE 8888-8890
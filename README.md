# Hello bootstrap

* [interesting django work](https://github.com/gaozhecn/myweb)
* Streamlit:
    * https://github.com/jkanner/streamlit-dataview
    * https://github.com/streamlit/example-app-time-series-annotation

* [Built-in template tags and filters](https://docs.djangoproject.com/en/4.0/ref/templates/builtins/)
    * https://pythonguides.com/django-for-loop/
```html
<body>
    <br>
    <div class="container w-50">
        <ul class="list-group">
            <li class="list-group-item active text-center font-weight-bold">Employee List</li>
            {% for employee in employee_list %}
            <li class="list-group-item"><b>{{ forloop.first }}</b>- {{ employee.name }}</li>
            {% endfor %}
        </ul>
    </div>
</body>
```
    * `cycle`: "The first iteration produces HTML that refers to class row1, the second to row2, the third to row1 again, and so on for each iteration of the loop."

```html
{% for o in some_list %}
    <tr class="{% cycle 'row1' 'row2' %}">
        ...
    </tr>
{% endfor %}
```
    * 

* [CLick to create a new page!](https://stackoverflow.com/questions/4907843/open-a-url-in-a-new-tab-and-not-a-new-window)
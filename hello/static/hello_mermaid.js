mermaid.initialize({startOnLoad: true, theme: 'forest', securityLevel: 'loose'});

$("#test").text(row_data);

var callback = function(){
    //alert('A callback was triggered');
    console.log("callback triggered");
    //alert(row_data);

    $("#details").text("Show python data after the click on mermaid chart: " + row_data);
    $(".clickable").append("<div class='mermaid'>graph LR \
                            A(Start)-->B(Do some stuff);");

}
from django.urls import include, re_path
from django.urls import path

from django.contrib import admin
admin.autodiscover()

import hello.views

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    re_path(r'^$', hello.views.index, name='index'),
    #re_path(r'^result/$', hello.views.result, name='result'),
    re_path(r'^subpage1/$', hello.views.subpage1, name='subpage1'),
    re_path(r'^subpage2/$', hello.views.subpage2, name='subpage2'),
    path('admin/', admin.site.urls),
]
